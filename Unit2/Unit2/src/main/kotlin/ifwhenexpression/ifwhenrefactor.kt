package ifwhenexpression

interface Expr

class Num(val value: Int) : Expr
class Sum(val left: Expr, val right: Expr) : Expr

fun main(args: Array<String>) {
    println(evalif(Sum(Sum(Num(20), Num(8)), Num(12))))
    println(evalwhen(Sum(Sum(Num(30), Num(8)), Num(12))))
    println(evalwhenblk(Sum(Sum(Num(40), Num(8)), Num(12))))
}

// Использование if в качестве выражения
fun evalif(e: Expr): Int =
    if (e is Num) {
        e.value
    } else if (e is Sum) {
        evalif(e.left) + evalif(e.right)
    } else {
        throw java.lang.IllegalArgumentException("Unknown expression")
    }

// Замена if на when
fun evalwhen(e: Expr): Int =
    when (e) {
        is Num -> e.value
        is Sum -> evalwhen(e.left) + evalwhen(e.right)
        else -> throw java.lang.IllegalArgumentException("Unknown expression")
    }

// Использование блоков в выражении when. Последняя строка неявно возвращает значение (вызывает return)
fun evalwhenblk(e: Expr): Int =
    when (e) {
        is Num -> {
            println("e.value: ${e.value}")
            e.value
        }
        is Sum -> {
            val leftval = evalwhenblk(e.left)
            val rightval = evalwhenblk(e.right)
            println("e.left: ${leftval}, e.right: ${rightval}")
            leftval + rightval
        }
        else -> throw java.lang.IllegalArgumentException("Unknown expression")
    }