// Использование цикла for.
package forwhileexpr

import java.util.TreeMap

fun main(args: Array<String>) {
    // Последовательный порядок цикла
//    for (i in 1..100)
//        println(fizzbuzz(i))

    // Обратный порядок цикла с определенным шагом
//    for (i in 100 downTo 1 step 4)
//        println(fizzbuzz(i))

    val binarysteps = TreeMap<Char, String>()
    for (c in 'A'..'Z') {
        val binary = Integer.toBinaryString(c.code)
        binarysteps[c] = binary
    }
    for (item in binarysteps) {
        println("item.key: ${item.key}, item.value: ${item.value}")
    }
}

fun fizzbuzz(i: Int) =
    when {
        i % 15 == 0 -> "FIZZBUZZ"
        i % 5 == 0 -> "FIZZ"
        i % 3 == 0 -> "BUZZ"
        else -> "$i "
    }
