// Пример использования объектов из другого пакета
package geometry.example

import geometry.shapes.createRandomRectangle

fun main(args: Array<String>){
    println("Height: ${createRandomRectangle().height}, Width: ${createRandomRectangle().width}")
    println("isSquare: ${createRandomRectangle().isSquare}")
}