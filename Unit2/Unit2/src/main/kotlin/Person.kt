// Класс написанный на языке Java. С помощью встроенных инструментов
// его можно легко преобразовать в код на Kotlin.
// Code -> Convert Java File to Kotlin File
/* public class Person {
    private final String name ;
    public Person(String name) {
        this.name = name ;
    }
    public String getName ( ) {
        return name ;
    }
} */

// Полученный класс после преобразования из Java в Kotlin
class Person(val name: String)