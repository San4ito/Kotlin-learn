// Использование перечислений + определение метода класса перечисления
package enumeration

enum class Color(val r: Int, val g: Int, val b: Int) {
    RED(255, 0, 0), YELLOW(255, 255, 0),
    VIOLET(238, 130, 238);

    fun toRGB() = (r * 256 + g) * 256 + b
}

fun main(args: Array<String>) {
    var enn = Color.YELLOW
    println("Color YELLOW: ${enn.toRGB()}")
    println("Color RED: ${Color.RED.toRGB()}")

    println(getMnemonic(Color.VIOLET))
    // Здесь порядок не такой как описан в условии when, но работает корректно, т.к. используется setOf
    println(mix(Color.YELLOW, Color.RED))
    println(mix(Color.YELLOW, Color.VIOLET))
}

// Использование when с классами перечисления. Так же в функции when испольщуется как выражение
fun getMnemonic(color: Color) =
    when (color) {
        Color.RED -> "Kajdyi"
        Color.YELLOW -> "Jelaet"
        Color.VIOLET -> "Fazan"
    }

// Использование различных объектов в ветках when. Функция неоптимизированна,
// так как создает каждый раз множество
fun mix(c1: Color, c2: Color) =
// Функция setOf создает множество Set с объектами, переданными в аргументах
    // Множество - это коллекция, порядок элементов которой не важен
    when (setOf(c1, c2)) {
        setOf(Color.RED, Color.YELLOW) -> "Orange"
        setOf(Color.RED, Color.VIOLET) -> "RedViolet"
        else -> throw Exception("Dirty color")
    }

// Использование when без аргументов. Условием выбора ветки может стать любое логическое выражение
fun mixOptimized(c1: Color, c2: Color) =
    when {
        (c1 == Color.RED && c2 == Color.YELLOW) ||
                (c1 == Color.YELLOW && c2 == Color.RED) -> "Orange"
        (c1 == Color.RED && c2 == Color.VIOLET) ||
                (c1 == Color.VIOLET && c2 == Color.RED) -> "RedViolet"
        else -> throw java.lang.Exception("Dirty color")
    }
