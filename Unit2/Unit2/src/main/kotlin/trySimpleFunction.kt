fun main(args: Array<String>) {
    println(max(3,6))
    println(max(7,2))
    println(max(5,5))
}

// Обычная функция
//fun max(a: Int, b: Int): Int {
//    return if (a > b) a else b
//}

// Та же самая функция, но ее можно написать в одну строку
fun max(a: Int, b: Int): Int = if (a > b) a else b